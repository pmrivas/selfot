<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>selfot</title>
<link href="https://fonts.googleapis.com/css?family=Bangers|Text+Me+One|Alfa+Slab+One|Bowlby+One+SC|Baloo" rel="stylesheet"> 
<link href="./micss.css" rel="stylesheet"> 
	<!-- Bootstrap Core CSS -->
	<link href="./bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

	<!-- MetisMenu CSS -->
	<link href="./bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

	<!-- Timeline CSS -->
	<link href="./dist/css/timeline.css" rel="stylesheet">

	<!-- DataTables CSS -->
	<link href="./bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

	<!-- DataTables Responsive CSS -->
	<link href="./bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

	<!-- Custom CSS -->
	<link href="./dist/css/sb-admin-2.css" rel="stylesheet">

	<!-- Morris Charts CSS -->
	<link href="./bower_components/morrisjs/morris.css" rel="stylesheet">

	<!-- Custom Fonts -->
	<link href="./bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

	<!-- DATEPICKER CSS -->
	<link href="./bower_components/Datepicker/css/bootstrap-datepicker.css" rel="stylesheet">

	<!-- DROPZONE CSS -->
	<link href="./css/dropzone.css" rel="stylesheet">
	<link href="./css/basic.css" rel="stylesheet">


	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>
		<div id="container">
			<div class="row">
				<div class="row">
					<div class="col-lg-2"></div>
					<div class="col-lg-8" align="right"><img src="./imag/logo.png" class="img-responsive" width="10%" onclick="return doLogOut();"></div>
					<div class="col-lg-2">&nbsp</div><br><br><br>
				</div>

<div class="row">
	<div class="col-lg-2" align="center"></div>
	<div class="col-lg-8">

		<div class="panel panel-default">
			<div class="panel-body">

				<br><br>
				 <div class="row">
		   
  
					<div class="col-lg-5" align="left">           
						<div id="containerFoto" style="background:url('./sf/img.php?fotografo=1');background-size:80% auto;background-repeat: no-repeat;background-position: center;">
							 <div class="row">
								<div class="col-lg-12" align="center" style="height: 7.3em;padding-top: 1em"></div>
							</div>
							 <div class="row" style="height: 3em">
								<div class="col-lg-2"></div>
								<div class="col-lg-10"></div> 
							</div>
							 <div class="row" style="height: 2em">
								<div class="col-lg-11" align="right">
									 <p style="font-family: 'Baloo', cursive;font-size: 1em;color: #ffffff"></p>
								</div>
								<div class="col-lg-1" align="right">&nbsp</div>
							</div>                    
						</div>
					</div>

					<div class="col-lg-4" align="left">
						<p class="pie" id="sfnombre"></p>
						 <p class="pie">Ultimo Evento: <span id="ultevento"></span></p>
						 <p class="pie">Cantidad Total de Fotos: <span id="fotostotales"></span></p>
						 <p class="pie">Cantidad Total Vendida: <span id="vendidas"></span></p>                        
					</div>
				</div> 
				<hr/>

				<ul class="nav nav-tabs">
					<li class="active"><a href="#subir" data-toggle="tab" aria-expanded="false"><p class="pie"> Subir Fotos</p></a></li>
					<li class=""><a href="#resumen" data-toggle="tab" aria-expanded="true"><p  class="pie"> Resumen</p></a></li>
					<li class=""><a href="#ventas" data-toggle="tab" aria-expanded="false"><p class="pie"> Ventas</p></a></li>
					<li class=""><a href="#datos" data-toggle="tab" aria-expanded="false"><p  class="pie"> Datos Personales</p></a></li>
				</ul>

				<div class="tab-content">
					<div class="tab-pane fade active in" id="subir">
						<h4>&nbsp</h4>
						<div class="row">
							<div class="col-md-6 col-md-offset-3" align="left">
									<div class="form-group" align="left">
										<p  style="font-family: 'Text Me One', sans-serif;font-size: 20px">Indique el evento de las fotos:</p>
										<select class="form-control" id="evid" name="evid">
										</select>
									</div>
									<hr />
							</div>
							<div class="col-md-12 " align="left" style="min-height:150px;">
								<div id="fotosdefotografo">
									<div id="actions" class="row">

										<div class="col-lg-7">
											<!-- The fileinput-button span is used to style the file input field as button -->
											<span class="btn btn-success fileinput-button">
													<i class="glyphicon glyphicon-plus"></i>
													<span>Agregar Archivos...</span>
											</span>
											<button type="submit" class="btn btn-primary start">
													<i class="glyphicon glyphicon-upload"></i>
													<span>Comenzar subida</span>
											</button>
											<button type="reset" class="btn btn-warning cancel">
													<i class="glyphicon glyphicon-ban-circle"></i>
													<span>Cancelar subida</span>
											</button>
										</div>
										<div class="col-lg-5">
											<!-- The global file processing state -->
											<span class="fileupload-process">
												<div id="total-progress" class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
													<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
												</div>
											</span>
										</div>
									</div>
								</div> 
								<div class="table table-striped" class="files" id="previews">
									<div id="template" class="file-row col-md-3">
										<!-- This is used as the file preview template -->
										<div class="col-md-6">
												<span class="preview"><img data-dz-thumbnail /></span>
										</div>
										<div class="col-md-6">
												<p class="name" data-dz-name></p>
												<strong class="error text-danger" data-dz-errormessage></strong>
										</div>
										<div class="col-md-6">
												<p class="size" data-dz-size></p>
												<button data-dz-remove class="btn btn-danger delete">
													<i class="glyphicon glyphicon-trash"></i>
													<span>Borrar</span>
												</button>
										</div>
										<div class="col-md-6">
												<div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
													<div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
												</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 ">
								<hr />
								<p  class="pie" align="right">Total de fotos a subir: <span id="totfotos"></span></p>
								<hr />
							</div>
						</div>
					</div>

					<div class="tab-pane fade " id="resumen">
						<div class="row">
							<div class="col-lg-12" align="left">
								<div class="table-responsive">
									<table class="table table-striped table-bordered table-hover">
										<thead>
											<tr>
												<th>#</th>
												<th>Foto</th>
												<th>Evento</th>
												<th>Disciplina</th>
												<th>Núm. Corredor</th>
												<th>Nombre Corredor</th>
												<th>Fec. Subida</th>
												<th>Ventas</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>1</td>
												<td align="center"><img src="./imag/portada.JPG" width="40vw"  ></div></td>
												<td><p class="pie">Triatlon Gente De la Pampa</p></td>
												<td align="center"><img src="./imag/icono bici.png" width="40vw"></td>
												<td>1049</td>
												<td>Daniel Vidoret</td>
												<td>28/04/1978</td>
												<td>77</td>
											</tr>
											<tr>
												<td>2</td>
												<td align="center"><img src="./imag/portada.JPG" width="40vw"  ></div></td>
												<td><p class="pie">Triatlon Gente De la Pampa</p></td>
												<td  align="center"><img src="./imag/icono bici.png" width="40vw"></td>
												<td>1049</td>
												<td>Daniel Vidoret</td>
												<td>28/04/1980</td>
												<td>34</td>
											</tr>
											<tr>
												<td>3</td>
												<td align="center"><img src="./imag/portada.JPG" width="40vw"  ></div></td>
												<td><p class="pie">Triatlon Gente De la Pampa</p></td>
												<td  align="center"><img src="./imag/icono bici.png" width="40vw"></td>
												<td>1049</td>
												<td>Daniel Vidoret</td>
												<td>28/04/1978</td>
												<td>634</td>
											</tr>
										</tbody>
									</table>
								</div>

							</div>
						</div>                    
					</div>

					<div class="tab-pane fade" id="ventas">
						<h4>ventas</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					</div>

					<div class="tab-pane fade" id="datos">
						<h4>Datos Personales</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					</div>

				</div>
			</div>

			<div class="panel-footer">
				<div class="row">
					<div class="col-lg-2"></div>
					<div class="col-lg-2"></div>
					<div class="col-lg-2"></div>
					<div class="col-lg-2"></div>
					<div class="col-lg-2"></div>
					<div class="col-lg-2"></div>
				</div>
			</div>
		</div>

	<div class="col-lg-2" align="center"></div>
</div>


<div class="row">
	<div class="col-lg-12" align="center">
		<hr/>
		<p  class="superdestacado">que foton sacaste  !!</p>
		<hr/>
	</div>
</div>
	   
	<!-- Modal -->
<div class="modal fade" id="modalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
	<form action="panelFotografos.php" method="post" id="loginfoto" onsubmit="return false;">
		<div class="panel panel-primary ">
		  <div class="panel-heading">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		  </div>
		  <div class="panel-body" id='contenidoModal2'>
				<div class="row">
					<div class="col-lg-12" align="center" style="padding-top: 0.7%"><input name="Usuario" id="Usuario" class="form-control" placeholder="Usuario"></div>
				</div>      
				<div class="row">
					<div class="col-lg-12" align="center" style="padding-top: 0.7%"><input name="Password" id="Password" type="password" class="form-control" placeholder="Contraseña"></div>
				</div>                  
				<div class="row">
					<div class="col-lg-12" align="center" style="padding-top: 0.7%">
						<button type="button" class="btn btn-default btn-circle btn-lg" style="color: #0F70B7"  onclick="ingresarUsContra()"><i class="fa fa-camera" style="color: #0F70B7"></i></button>
					</div>
				</div>              
		  </div>
		</div>
	</form>
  </div>
</div>

	<!-- jQuery -->
 <script src="./bower_components/jquery/dist/jquery.min.js"></script>

	<script src="./bower_components/jquery/dist/jquery-ui.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="./bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- Metis Menu Plugin JavaScript -->
	<script src="./bower_components/metisMenu/dist/metisMenu.min.js"></script>

	<!-- DataTables JavaScript -->
	<script src="./bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
	<script src="./bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
	<script src="./bower_components/datatables-responsive/js/dataTables.responsive.js"></script>

	<!-- DATEPICKER -->
	<script src="./bower_components/Datepicker/js/bootstrap-datepicker.js"></script>


	<!-- Custom Theme JavaScript -->
	<script src="./dist/js/sb-admin-2.js"></script>

	<script src="./codigo/docentesXmateria/funciones.js"></script>
	<!-- dropzone -->
	<script src="./js/dropzone.js"></script>
	<script src="./js/fotografos.js"></script>



<script type="text/javascript">
var queMacros="./sf/sfmacros.php";
Dropzone.autoDiscover = false;
var previewNode = document.querySelector("#template");
previewNode.id = "";
var previewTemplate = previewNode.parentNode.innerHTML;
previewNode.parentNode.removeChild(previewNode);
var myDropZone;

$(function(){
	traexDefecto();
});

function iniDz() {
	myDropzone = new Dropzone(document.body, {
		url: "./subefotos.php", clickable:".fileinput-button",
		previewTemplate: previewTemplate,resizeWidth:2000,autoQueue:false,
		previewsContainer: "#previews",acceptedFiles:"image/jpeg",
		success: function (file, response) {
			var imgName = response;
			file.previewElement.classList.add("dz-success");
		},
		error: function (file, response) {
			file.previewElement.classList.add("btn-danger"); //dz-error 
			file.previewElement.setAttribute('title',response);
		}
	});
	myDropzone.on("sending", function(file, xhr, formData) {
	  // Will send the filesize along with the file as POST data.
	  formData.append("evid", $('#evid').val());
	});
	myDropzone.on("error", function (file) {

	})
	myDropzone.on("addedfile", function(file) {
	  // Hookup the start button
	  //file.previewElement.querySelector(".start").onclick = function() { myDropzone.enqueueFile(file); };
	});
	myDropzone.on("totaluploadprogress", function(progress) {
	  document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
	});

	myDropzone.on("sending", function(file) {
	  // Show the total progress bar when upload starts
	  document.querySelector("#total-progress").style.opacity = "1";
	  // And disable the start button
	  //file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
	});

	// Hide the total progress bar when nothing's uploading anymore
	myDropzone.on("queuecomplete", function(progress) {
	  document.querySelector("#total-progress").style.opacity = "0";
	});

	// Setup the buttons for all transfers
	// The "add files" button doesn't need to be setup because the config
	// `clickable` has already been specified.
	document.querySelector("#actions .start").onclick = function() {
	  myDropzone.enqueueFiles(myDropzone.getFilesWithStatus(Dropzone.ADDED));
	};
	document.querySelector("#actions .cancel").onclick = function() {
	  myDropzone.removeAllFiles(true);
	};
/*
	myDropzone.on("complete", function(file) {
	  myDropzone.removeFile(file);
	});*/
}

function traexDefecto() {
	$.ajax({type: "get",url: queMacros + "?xdefecto=1",dataType: 'json',cache: false,success: function(datos, textStatus, jqXHR) {
		if (datos && datos.dologin) {
			$('#modalLogin').modal('show');
		} else if (datos) {
			return cargarDefectos(datos);
		}
	}});
}
function doLogOut() {
	$.ajax({type: "get",url: queMacros + "?dologout=1",dataType: 'json',cache: false,success: function(datos, textStatus, jqXHR) {
		location.href="./";
	}});
}
function ingresarUsContra(){
	$.ajax({type: "post",data: {Usuario:$('#Usuario').val(),Password:$('#Password').val()},url: queMacros + "?dologin=1",dataType: 'json',cache: false,success: function(datos, textStatus, jqXHR) {
		if (datos && datos.err) {
			$('#modalLogin').effect( "shake" );
			$('#Usuario').focus().select();
		} else if (datos) {
			$('#modalLogin').modal('hide');
			cargarDefectos(datos);
		}
	}});
}
function cargarDefectos(datos) {
	$('#containerFoto').css('background-image', 'url(./sf/img.php?fotografo=1&' + Math.random() + ')');
	$('#evid').html('');
	$(datos.eventos).each(function (i,k) {
		$('#evid').append('<option value=' + k.evid + '>' + k.evnombre + '</option>');
	});
	iniDz();
}
</script>
</body>
</html>