<?php
session_start();
require './pruebas/keys.php';
require 'aws/aws-autoloader.php';
require_once("./sfconnect.php");
require_once("./sf/sflibs.php");
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;
use Aws\Rekognition\RekognitionClient;
if (!empty($_FILES)) {
  $tempFile = $_FILES['file']['tmp_name'];          //3             
	$s3 = new S3Client([
	    'region' => 'us-east-1',
	    'version' => 'latest',
	    'credentials' => [
	        'key'    => $key,
	        'secret' => $secret
	    ]
	]);
  try {
		ini_set("gd.jpeg_ignore_warning", 1);
		$evid=$_POST['evid']*1;
		$md5=md5_file($tempFile);
		if ((exif_imagetype($tempFile) != IMAGETYPE_JPEG) or (imagecreatefromjpeg( $tempFile ) ==false  )) {
			http_response_code(500);
			die("Esta imagen no pudo ser leida");
		}
		$nombre=sanitiza($_FILES['file']['name'],"string");
		$carpeta=$_SESSION['sfusid'] . "/" . $evid . ".{$md5}.jpg";
		$hay=$conn1->GetOne('Select ftid from sffotos where ftevid=? and ftusid=? and ftmd5=?',array($evid,$_SESSION['sfusid'],$md5));
		if ($hay) {
			http_response_code(500);
			die("Imagen Repetida");
		}
		$result = $s3->putObject([
			'Bucket'     => $bucket,
			'Key'        => $carpeta,
			'SourceFile' => $tempFile,
			'ACL'    => 'public-read',
			'ContentType' => $_FILES['file']['type']
    ]);
		$conn1->Execute('insert into sffotos (ftevid,ftusid,ftnombre,ftubicacion,ftenctype,ftfecha,ftmd5) values (?,?,?,?,?,now(),?)',
			array($evid,$_SESSION['sfusid'],$nombre,$result['ObjectURL'],$_FILES['file']['type'],$md5));
		$ftid=$conn1->insert_Id();
	$rekognition = RekognitionClient::factory(array(
			'region'	=> 'us-east-1',
			'version'	=> 'latest',
	    'credentials' => [
	        'key'    => $key,
	        'secret' => $secret
	    ]
		));
		// now we need to run rekognition over this
		$labels = $rekognition->detectText([
			'Image'		=> [
				'S3Object'	=> [
					'Bucket'	=> $bucket,
					'Name'		=>  $carpeta,
					],
				],
		]);
		foreach($labels as $k=>$v) {
			if (is_array($v)) {
				foreach($v as $kk=>$vv) {
					if (is_numeric($kk)) {
						if ((!empty($vv['Confidence'])) and ($vv['Confidence']*1>85)) {
							if ($vv['DetectedText']!="80K") {
								$numero=$vv['DetectedText']*1;
								if ($numero) $conn1->Execute('insert into sfnumfots (nfftid,nfnumero,nfconfidence,nfgeom) values (?,?,?,?)',array($ftid,$numero,$vv['Confidence'],json_encode($vv['Geometry'])));
							}
						}
					}
				}
			}
		}
    die("OK");
	} catch (S3Exception $e) {
    error_log("uff: ".$e->getMessage());
	}
     
}


?>