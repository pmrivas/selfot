<?php
	session_start();
	header('Content-type: image/png');
	if (isset($_GET['fotografo'])) {
		$png_image = imagecreatefrompng("../imag/fotofoto.png");
		$white = imagecolorallocate($png_image, 255, 255, 255);
		$font_path = '../css/Bangers-Regular.ttf';
		$text = (empty($_SESSION['sfnombre'])) ? " " : $_SESSION['sfnombre'];
		imagettftext($png_image, 125, 0, 475, 900, $white, $font_path, $text);
		imagepng($png_image);
		imagedestroy($png_image);
	}
?>