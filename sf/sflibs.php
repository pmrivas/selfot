<?php
/*************************
** sflibs.php: Librerias varias...
**************************/
function sanitiza($que,$como) {
	if ($como=="string") $que=filter_var($que, FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW);
	else if ($como=="mail") $que=(filter_var(filter_var($que,FILTER_SANITIZE_EMAIL),FILTER_VALIDATE_EMAIL)) ? filter_var($que,FILTER_SANITIZE_EMAIL) : false;
	else if ($como=="num") $que=filter_var($que, FILTER_SANITIZE_NUMBER_INT, FILTER_FLAG_STRIP_LOW);
	return $que;
}
function creaSesion($Usuario, $Password) {
	global $conn1;
	$usid=false;
	@extract($conn1->GetRow('Select usid,ususuario, usnombre, usfacebook, uscategoria from sfusuarios where ususuario=? and uspassword=sha1(?)',array($Usuario,$Password)));
	if ($usid) {
		$_SESSION['sflogeado']=1;
		$_SESSION['sfusuario']=$ususuario;
		$_SESSION['sfnombre']=$usnombre;
		$_SESSION['sffacebook']=$usfacebook;
		$_SESSION['sfcategoria']=$uscategoria;
		$_SESSION['sfusid']=$usid;
	} else {
		unset($_SESSION['sflogeado']);
		unset($_SESSION['sfusuario']);
		unset($_SESSION['sfnombre']);
		unset($_SESSION['sffacebook']);
		unset($_SESSION['sfcategoria']);
	}
}
function traeEventos() {
	global $conn1;
	return $conn1->GetAll('Select * from sfeventos');
}
?>