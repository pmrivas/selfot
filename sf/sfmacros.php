<?php
/*******************
** sfmacros: como no podía ser de otra manera :D
********************/
session_start();
require_once("../sfconnect.php");
require_once("./sflibs.php");
header("Content-type:application/json");
if (!empty($_GET['dologout'])) {
	unset($_SESSION['sflogeado']);
	unset($_SESSION['sfusuario']);
	unset($_SESSION['sfnombre']);
	unset($_SESSION['sffacebook']);
	unset($_SESSION['sfcategoria']);
	die(json_encode(array("ok"=>1)));
}
if (!empty($_GET['dologin'])) {
	$Usuario=sanitiza($_POST['Usuario'],'email');
	$Password=sanitiza($_POST['Password'],'string');
	creaSesion($Usuario,$Password);
	$_GET['xdefecto']=1;
}
if (!empty($_GET['xdefecto'])) {
	if (empty($_SESSION['sflogeado'])) {
		die(json_encode(array('err'=>1,'txerr'=>'No Ingresado','dologin'=>1)));
	}
	$rpta=array(
		'sfusuario'=>$_SESSION['sfusuario'],
		'sfnombre'=>$_SESSION['sfnombre'],
		'sffacebook'=>$_SESSION['sffacebook'],
		'sfcategoria'=>$_SESSION['sfcategoria'],
		'eventos'=>traeEventos(),
		'ok'=>1,
		'err'=>0
	);
	die(json_encode($rpta));
}

?>